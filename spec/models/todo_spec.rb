require 'rails_helper'

# Test suite for the Todo_ model
RSpec.describe Todo, type: :model do
  # TodoモデルがItemモデルと1:mの関係にある
  it { should have_many(:items).dependent(:destroy) }
  # 保存前にtitleカラムとcreated_byカラムが存在する
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:created_by) }
end
