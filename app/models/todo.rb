class Todo < ApplicationRecord
  # 複数のitemをもつ　Todoが削除される時にitemも削除する
  has_many :items, dependent: :destroy

  # 保存前にtitleカラムとcreated_byカラムは空のままで保存されない
  validates_presence_of :title, :created_by
end
