class Item < ApplicationRecord
  # todoモデル1つと関係を持つ
  belongs_to :todo
  # nameカラムは空のまま保存されない
  validates_presence_of :name
end
