# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:
# Version
* Ruby version
  * 2.5.0
* Rails version
  * 5.2.3

# System dependencies
* Need MySQL@5.7

* Configuration

`bundle install --path vendor/bundle`
* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

# Tips
## mysql2がインストールできない
ref: https://ty-engineer.com/ruby-on-rails/mysql-in-rails/
`$ brew info openssl`
の内容を読んで、LDFLAGSとCPPFLAGSのパスを通す。
その後、
`$ bundle config --local build.mysql2 "--with-ldflags=-L/usr/local/opt/openssl/lib --with-cppflags=-I/usr/local/opt/openssl/include"
`

`$ sudo bundle install --path vendor/bundle`
